
/**
 * App for overcloud
 * @type angular.module.angular-1_3_6_L1749.moduleInstance
 */
var App = angular.module('App', ["ui.router", "angular-jwt"]);

/**
 * jwt config,
 * set routing
 */
App.config(function ($stateProvider, $urlRouterProvider,
        $httpProvider, jwtOptionsProvider) {


    jwtOptionsProvider.config({
        
        authPrefix: '',
        tokenGetter: function (options, jwtHelper) {
            return window.localStorage.getItem('token');
        },
        whiteListedDomains: ['api.netskills.pl']
    });

    $stateProvider.state("login", {
        url: "/login",
        controller: "loginController",
        templateUrl: "templates/login.html"
    }).state("register", {
        url: "/register",
        controller: "registerController",
        templateUrl: "templates/register.html"
    }).state("logout", {
        url: "/logout",
        template: "logout",
        controller: function ($state) {
            window.localStorage.removeItem('token');
            $state.go('login');
        }
    }).state("home", {
        url: "/home",
        controller: "mainController",
        templateUrl: "templates/home.html"
    });
    $urlRouterProvider.otherwise('/home');

    $httpProvider.interceptors.push('jwtInterceptor');
});

App.constant('apiAddress', 'http://api.netskills.pl');
App.constant('defaultError', 'Sorry, something went wrong');


/**
 * run app
 * check user is auth
 * global helper function
 */
App.run(function ($rootScope, jwtHelper, $state) {

    var user = false;

    
    $rootScope.$on('$locationChangeSuccess', function () {
        if (!window.localStorage.getItem('token')) {
            user = false;
        }
    });
    /**
     * check user
     * @returns {Boolean|token}
     */
    $rootScope.getUser = function () {
        if (!user) {
            if (window.localStorage.getItem('token')) {
                var token = jwtHelper.decodeToken(window.localStorage.getItem('token'));

                if (token.hasOwnProperty('name')) {
                    user = token;
                }
            }
        }
        return user;
    };
    
    /**
     * helper for execute promise
     * @param {object} data
     * @param {local $scope} scope
     * @param {function} successCallback
     * @returns void
     */
    $rootScope.executeResult = function (data, scope, successCallback) {
        if (data.status === 1) {
            successCallback(data, scope);
        } else if (data.status === 0) {
            if (data.error.hasOwnProperty('errors')) {
                angular.forEach(data.error.errors, function (value) {
                    scope.errors.push(value.message);
                });
            } else {
                scope.errors.push(data.error.message);
            }
        } else {
            scope.errors.push('Sorry, something went wrong');
            scope.post.info = 'Sorry, something went wrong';
        }
        scope.isLoading = false;
    };
    
    $rootScope.logout = function(){
        window.localStorage.removeItem('token');
        $state.go('login');
    };
    console.log($rootScope.getUser());
});
/**
 * service post model
 */
App.service('postService',function($http,$q,apiAddress,promiseService){
   var service = {};
   
   service.getPosts = function(query){
      return promiseService.getPromise('GET',apiAddress + '/post',query);
   };
   
   service.addPost = function(post){
      return promiseService.getPromise('POST',apiAddress + '/post',post); 
   };
   
   service.updatePost = function(id,post){
     
      return promiseService.getPromise('PUT',apiAddress+'/post/'+id,post);
   };
   
   service.deletePost = function(id){
      return promiseService.getPromise('DELETE',apiAddress+'/post/'+id,{});
   };
   
   
   return service;
});

App.service('promiseService',function($http,$q){
   var service = {};
   /**
    * promise global helper
    * @param {string} method
    * @param {string} address
    * @param {object} data
    * @returns {.$q@call;defer.promise}
    */
   service.getPromise = function(method,address,data){
      var defered = $q.defer();
      $http({
          url: address,
          method: method,
          data: (method !== 'GET')? data: {},
          params: (method === 'GET')? data : {}
      }).success(function(data){
          defered.resolve(data);
      }).error(function(data){
          defered.reject(data);
      });
      return defered.promise;
   };
   
   
   return service;
});
/**
 * service user model
 */
App.service('userService',function($http,apiAddress,promiseService){
   var service = {};
   
   
   service.addUser = function(name,password){
      return promiseService.getPromise('POST',apiAddress + '/user',{"name":name,"password":password});
   };
   
   service.login = function(name,password){
       return promiseService.getPromise('POST',apiAddress + '/auth',{'name':name,'password':password});
   };
   
   service.getUsers = function(){
       return promiseService.getPromise('GET',apiAddress + '/user',{});

   };
   
   
   return service;
});
var compareTo = function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.compareTo = function(modelValue) {

                return modelValue === scope.otherModelValue;
            };
 
            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
};
 
App.directive("compareTo", compareTo);

/**
 * login controller
 */
App.controller('loginController', function ($scope, userService, $state, $rootScope) {

    if($rootScope.getUser()){
        $state.go('home');
    }

    $scope.name = null;
    $scope.password = null;
    $scope.isLoading = false;
    $scope.errors = [];
    $scope.submit = function () {
    /**
     * promise login user
     */
    userService.login($scope.name, $scope.password).then(function (data) {
        $scope.errors = [];    
        //console.log(data);
        $rootScope.executeResult(data, $scope, function (data, scope) {
            var token = data.result.token;
            window.localStorage.setItem("token", token);
            $state.go('home');
        });
     
        }).catch(function (result) {
            $scope.errors.push("Error Connection");
            $scope.isLoading = false;
        });
    };
});

/**
 * main controller, get post, CRUD post
 */
App.controller('mainController', function ($scope, postService, $rootScope, defaultError, $location) {



    $scope.initPost = function () {
        return {
            header: 'New Post',
            info: false
        };
    };

    $scope.getUser = function () {
        return $rootScope.getUser();
    }

    $scope.post = $scope.initPost();
    $scope.errors = [];
    $scope.isLoading = false;
    $scope.savePost = function () {
        $scope.errors = [];
        $scope.post.info = false;
        $scope.isLoading = true;
        if ($rootScope.getUser()) {

            if ($scope.post.hasOwnProperty('_id') && $scope.post._id !== '') {
                postService.updatePost($scope.post._id, $scope.post.params).then(function (data) {
                    
                    $rootScope.executeResult(data, $scope, function (data, scope) {
                       
                        scope.posts[data.result._id] = data.result;
                        scope.posts[data.result._id].user = scope.getUser();

                        scope.post = scope.initPost();
                        scope.post.info = 'SUCCESS';
                        getP();
                    });

                }).catch(function (data) {
                    $scope.post.info = defaultError;
                    $scope.isLoading = false;
                });
            } else {
                postService.addPost($scope.post.params).then(function (data) {
                    $rootScope.executeResult(data, $scope, function (data, scope) {
                        let new_post = data.result;
                        new_post.user = scope.getUser();

                        scope.posts[new_post._id] = new_post;

                        scope.post = scope.initPost();
                        scope.post.info = 'SUCCESS';
                        getP();
                    });

                }).catch(function (data) {
                    $scope.post.info = defaultError;
                    $scope.isLoading = false;
                });
                ;
            }
        } else {
            $rootScope.logout();
        }
    };

    $scope.selectPost = function (post) {
        $scope.post.info = false;

        $scope.post = {
            header: 'Edit post ',
            _id: post._id,
            params: {
                title: post.title,
                text: post.text
            }
        };
        $location.hash('ff');
    };

    $scope.deletePost = function (post) {
        if (post.user._id === $rootScope.getUser()._id) {
            postService.deletePost(post._id).then(function (data) {
                if (data.status === 1) {
                    delete $scope.posts[post._id];
                } else {
                    $scope.post.info = defaultError;
                }
                $scope.isLoading = false;
                getP();
            }).catch(function (data) {
                $scope.post.info = defaultError;
                $scope.isLoading = false;
            });
        }
    };

    $scope.posts = {};
    $scope.like = '';
    
    $scope.query = {
        'query': {
         
        },
        'limit': 10,
        'sort': {'created_at': -1}

    };

   
    $scope.$watch('query', function () {
        getP();
        
    },true);


    $scope.getSort = function () {
        var prop = $scope.query.sort[Object.keys($scope.query.sort)[0]];
        var prefix = '';
        if ($scope.query.sort[prop] === -1) {
            prefix = '-';
        }
        return prefix + $scope.query.sort[prop];
    };

    var getP = function () {
        postService.getPosts($scope.query).then(function (data) {
            $scope.posts = {};
            if (data.status === 1) {
              
                angular.forEach(data.result, function (val) {
                    $scope.posts[val._id] = val;
                });
            }
           
        });
    };
    getP();

});

/**
 * register controller
 */
App.controller('registerController', function ($scope, $state, userService, defaultError, $rootScope) {
   
    if($rootScope.getUser()){
        $state.go('home');
    }
    $scope.info = '';
    $scope.isLoading = false;
    $scope.errors = [];
    $scope.submit = function () {
        $scope.errors = [];
        $scope.isLoading = true;
        /**
         * promise add new user
         */
        userService.addUser($scope.username, $scope.password).then(function (result) {
            $rootScope.executeResult(result,$scope,function(result,scope){
                 $scope.info = 'Hello ' + result.result.name + '. You can log in';
            });
           
        }).catch(function (result) {
            console.log(result);
            $scope.errors.push("Error Connection");
            $scope.isLoading = false;
        });
    };
});