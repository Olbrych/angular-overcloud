
App.service('promiseService',function($http,$q){
   var service = {};
   /**
    * promise global helper
    * @param {string} method
    * @param {string} address
    * @param {object} data
    * @returns {.$q@call;defer.promise}
    */
   service.getPromise = function(method,address,data){
      var defered = $q.defer();
      $http({
          url: address,
          method: method,
          data: (method !== 'GET')? data: {},
          params: (method === 'GET')? data : {}
      }).success(function(data){
          defered.resolve(data);
      }).error(function(data){
          defered.reject(data);
      });
      return defered.promise;
   };
   
   
   return service;
});