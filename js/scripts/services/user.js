/**
 * service user model
 */
App.service('userService',function($http,apiAddress,promiseService){
   var service = {};
   
   
   service.addUser = function(name,password){
      return promiseService.getPromise('POST',apiAddress + '/user',{"name":name,"password":password});
   };
   
   service.login = function(name,password){
       return promiseService.getPromise('POST',apiAddress + '/auth',{'name':name,'password':password});
   };
   
   service.getUsers = function(){
       return promiseService.getPromise('GET',apiAddress + '/user',{});

   };
   
   
   return service;
});