/**
 * service post model
 */
App.service('postService',function($http,$q,apiAddress,promiseService){
   var service = {};
   
   service.getPosts = function(query){
      return promiseService.getPromise('GET',apiAddress + '/post',query);
   };
   
   service.addPost = function(post){
      return promiseService.getPromise('POST',apiAddress + '/post',post); 
   };
   
   service.updatePost = function(id,post){
     
      return promiseService.getPromise('PUT',apiAddress+'/post/'+id,post);
   };
   
   service.deletePost = function(id){
      return promiseService.getPromise('DELETE',apiAddress+'/post/'+id,{});
   };
   
   
   return service;
});