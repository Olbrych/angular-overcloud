
/**
 * register controller
 */
App.controller('registerController', function ($scope, $state, userService, defaultError, $rootScope) {
   
    if($rootScope.getUser()){
        $state.go('home');
    }
    $scope.info = '';
    $scope.isLoading = false;
    $scope.errors = [];
    $scope.submit = function () {
        $scope.errors = [];
        $scope.isLoading = true;
        /**
         * promise add new user
         */
        userService.addUser($scope.username, $scope.password).then(function (result) {
            $rootScope.executeResult(result,$scope,function(result,scope){
                 $scope.info = 'Hello ' + result.result.name + '. You can log in';
            });
           
        }).catch(function (result) {
            console.log(result);
            $scope.errors.push("Error Connection");
            $scope.isLoading = false;
        });
    };
});