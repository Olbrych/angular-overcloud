
/**
 * main controller, get post, CRUD post
 */
App.controller('mainController', function ($scope, postService, $rootScope, defaultError, $location) {



    $scope.initPost = function () {
        return {
            header: 'New Post',
            info: false
        };
    };

    $scope.getUser = function () {
        return $rootScope.getUser();
    }

    $scope.post = $scope.initPost();
    $scope.errors = [];
    $scope.isLoading = false;
    $scope.savePost = function () {
        $scope.errors = [];
        $scope.post.info = false;
        $scope.isLoading = true;
        if ($rootScope.getUser()) {

            if ($scope.post.hasOwnProperty('_id') && $scope.post._id !== '') {
                postService.updatePost($scope.post._id, $scope.post.params).then(function (data) {
                    
                    $rootScope.executeResult(data, $scope, function (data, scope) {
                       
                        scope.posts[data.result._id] = data.result;
                        scope.posts[data.result._id].user = scope.getUser();

                        scope.post = scope.initPost();
                        scope.post.info = 'SUCCESS';
                        getP();
                    });

                }).catch(function (data) {
                    $scope.post.info = defaultError;
                    $scope.isLoading = false;
                });
            } else {
                postService.addPost($scope.post.params).then(function (data) {
                    $rootScope.executeResult(data, $scope, function (data, scope) {
                        let new_post = data.result;
                        new_post.user = scope.getUser();

                        scope.posts[new_post._id] = new_post;

                        scope.post = scope.initPost();
                        scope.post.info = 'SUCCESS';
                        getP();
                    });

                }).catch(function (data) {
                    $scope.post.info = defaultError;
                    $scope.isLoading = false;
                });
                ;
            }
        } else {
            $rootScope.logout();
        }
    };

    $scope.selectPost = function (post) {
        $scope.post.info = false;

        $scope.post = {
            header: 'Edit post ',
            _id: post._id,
            params: {
                title: post.title,
                text: post.text
            }
        };
        $location.hash('ff');
    };

    $scope.deletePost = function (post) {
        if (post.user._id === $rootScope.getUser()._id) {
            postService.deletePost(post._id).then(function (data) {
                if (data.status === 1) {
                    delete $scope.posts[post._id];
                } else {
                    $scope.post.info = defaultError;
                }
                $scope.isLoading = false;
                getP();
            }).catch(function (data) {
                $scope.post.info = defaultError;
                $scope.isLoading = false;
            });
        }
    };

    $scope.posts = {};
    $scope.like = '';
    
    $scope.query = {
        'query': {
         
        },
        'limit': 10,
        'sort': {'created_at': -1}

    };

   
    $scope.$watch('query', function () {
        getP();
        
    },true);


    $scope.getSort = function () {
        var prop = $scope.query.sort[Object.keys($scope.query.sort)[0]];
        var prefix = '';
        if ($scope.query.sort[prop] === -1) {
            prefix = '-';
        }
        return prefix + $scope.query.sort[prop];
    };

    var getP = function () {
        postService.getPosts($scope.query).then(function (data) {
            $scope.posts = {};
            if (data.status === 1) {
              
                angular.forEach(data.result, function (val) {
                    $scope.posts[val._id] = val;
                });
            }
           
        });
    };
    getP();

});