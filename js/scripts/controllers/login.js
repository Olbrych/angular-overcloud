
/**
 * login controller
 */
App.controller('loginController', function ($scope, userService, $state, $rootScope) {

    if($rootScope.getUser()){
        $state.go('home');
    }

    $scope.name = null;
    $scope.password = null;
    $scope.isLoading = false;
    $scope.errors = [];
    $scope.submit = function () {
    /**
     * promise login user
     */
    userService.login($scope.name, $scope.password).then(function (data) {
        $scope.errors = [];    
        //console.log(data);
        $rootScope.executeResult(data, $scope, function (data, scope) {
            var token = data.result.token;
            window.localStorage.setItem("token", token);
            $state.go('home');
        });
     
        }).catch(function (result) {
            $scope.errors.push("Error Connection");
            $scope.isLoading = false;
        });
    };
});