
/**
 * App for overcloud
 * @type angular.module.angular-1_3_6_L1749.moduleInstance
 */
var App = angular.module('App', ["ui.router", "angular-jwt"]);

/**
 * jwt config,
 * set routing
 */
App.config(function ($stateProvider, $urlRouterProvider,
        $httpProvider, jwtOptionsProvider) {


    jwtOptionsProvider.config({
        
        authPrefix: '',
        tokenGetter: function (options, jwtHelper) {
            return window.localStorage.getItem('token');
        },
        whiteListedDomains: ['api.netskills.pl']
    });

    $stateProvider.state("login", {
        url: "/login",
        controller: "loginController",
        templateUrl: "templates/login.html"
    }).state("register", {
        url: "/register",
        controller: "registerController",
        templateUrl: "templates/register.html"
    }).state("logout", {
        url: "/logout",
        template: "logout",
        controller: function ($state) {
            window.localStorage.removeItem('token');
            $state.go('login');
        }
    }).state("home", {
        url: "/home",
        controller: "mainController",
        templateUrl: "templates/home.html"
    });
    $urlRouterProvider.otherwise('/home');

    $httpProvider.interceptors.push('jwtInterceptor');
});

App.constant('apiAddress', 'http://api.netskills.pl');
App.constant('defaultError', 'Sorry, something went wrong');


/**
 * run app
 * check user is auth
 * global helper function
 */
App.run(function ($rootScope, jwtHelper, $state) {

    var user = false;

    
    $rootScope.$on('$locationChangeSuccess', function () {
        if (!window.localStorage.getItem('token')) {
            user = false;
        }
    });
    /**
     * check user
     * @returns {Boolean|token}
     */
    $rootScope.getUser = function () {
        if (!user) {
            if (window.localStorage.getItem('token')) {
                var token = jwtHelper.decodeToken(window.localStorage.getItem('token'));

                if (token.hasOwnProperty('name')) {
                    user = token;
                }
            }
        }
        return user;
    };
    
    /**
     * helper for execute promise
     * @param {object} data
     * @param {local $scope} scope
     * @param {function} successCallback
     * @returns void
     */
    $rootScope.executeResult = function (data, scope, successCallback) {
        if (data.status === 1) {
            successCallback(data, scope);
        } else if (data.status === 0) {
            if (data.error.hasOwnProperty('errors')) {
                angular.forEach(data.error.errors, function (value) {
                    scope.errors.push(value.message);
                });
            } else {
                scope.errors.push(data.error.message);
            }
        } else {
            scope.errors.push('Sorry, something went wrong');
            scope.post.info = 'Sorry, something went wrong';
        }
        scope.isLoading = false;
    };
    
    $rootScope.logout = function(){
        window.localStorage.removeItem('token');
        $state.go('login');
    };
    console.log($rootScope.getUser());
});