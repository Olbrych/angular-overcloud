// wrapper function
module.exports = function (grunt) {
    // load all our Grunt plugins
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // task configuration goes here

        concat: {
            dist: {
                src: [
                    'js/scripts/module.js',
                    'js/scripts/services/{,*/}*.js',
                    'js/scripts/directives/{,*/}*.js',
                    'js/scripts/controllers/{,*/}*.js'],
                dest: "js/app.js"
            }
        },
        watch: {
            scripts: {
                files: '**/*.js',
                tasks: ['concat'],
                
            },

        }
    });



    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('default', ['concat']);

};
